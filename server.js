'use strict';

const express = require('express');
const axios = require('axios');

// На будущее. Для валидации и зачистки входящей информации.
// https://express-validator.github.io/docs/index.html

const BOT_INTERFACE = process.env.BOT_INTERFACE || '127.0.0.1';
const BOT_PORT = process.env.BOT_PORT || 3000;
// app.set('port', process.env.BOT_PORT || 3000);

const app = express();

app.use(express.json());                          // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true }));  // to support URL-encoded bodies

const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;
const BACKEND_API_URL = process.env.BACKEND_API_URL || 'https://vodonos.roboto.pro/strapi/api';

const BACKEND_BOT_TOKEN = process.env.BACKEND_BOT_TOKEN;

///// Блок функций для отправки /////

let send = (method, data) => {
  axios.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/${method}`, data)
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error.data);
  });
}

let sendMessage = data => {
  send(
    'sendMessage',
    data
  );
}

let sendSticker = data => {
  send(
    'sendSticker',
    data
  );
}

let createTransfer = (body) => {
  console.log('createTransfer');
  let chat_id = body.message ? body.message.chat.id : body.callback_query.message.chat.id;

  // Выяснено, что не у всех пользователей есть поле username
  let user = undefined;
  if (body.message) {
    user = body.message.from;
  }
  else if (body.callback_query) {
    user = body.callback_query.from;
  }

  let confirm_button_title = 'Спасибо'; // Изначально было "Подтвердить".

  let telegram_user_id = user.id;
  console.log('telegram_user_id:', telegram_user_id);
  
  // let username = "username" in user ? user.username : undefined;

  // if (! username) {
  //   sendMessage({
  //     chat_id: chat_id,
  //     text: `У пользователя с ID "${user.id}" (${user.first_name}${user.last_name ? ' ' + user.last_name : ''})` +
  //           ` не указано имя пользователя в поле username. Поэтому он не может делать записи о доставке воды.` +
  //           ` Установите имя пользователя или сообщите об этом администратору.`
  //   });
  //   return;
  // }

  // Отправить заявку в Страпи
  axios.post(
    `https://vodonos.roboto.pro/strapi/api/transfers/bybot?populate=*&pagination[pageSize]=1&sort=date:desc`,
    {
      data: {
        telegram_user_id: telegram_user_id
      }
    },
    {
      headers: {
        Authorization:
          `Bearer ${BACKEND_BOT_TOKEN}`,
      }
    }
  )
  .then(function (response) {
    console.log('RESPONSE DATA');
    console.log(response.data);
    // Отправлено нормальное имя УЗ
    // {
    //   data: {
    //     id: 13,
    //     attributes: {
    //       date: '2022-07-18T06:49:14.155Z',
    //       confirmed: false,
    //       createdAt: '2022-07-18T06:49:14.159Z',
    //       updatedAt: '2022-07-18T06:49:14.159Z',
    //       confirmationThreshold: 4
    //     }
    //   },
    //   meta: {}
    // }
    
    if (
      'status' in response.data
      && response.data.status === 400
    ) {
      sendMessage({
        chat_id: chat_id,
        text: `Сервер Strapi вернул ошибку "${response.data.message}".`
      });
      return;
    }

    let delivery_id = response.data.data.id;

    sendMessage({
      chat_id: chat_id,
      text: `${user.first_name}${user.last_name ? ' ' + user.last_name : ''} заявляет, что он выполнил доставку №${delivery_id} и принёс новую 20-литровую бутылку с водой!`,
      reply_markup: {
        inline_keyboard: [
          [
            // {text: 'Отклонить', callback_data: `declineTransfer_${delivery_id}`},
            {text: confirm_button_title, callback_data: `confirmTransfer_${delivery_id}`},
          ]
        ]
      }
    });

    // https://core.telegram.org/bots/api#formatting-options
    // send(
    //   'sendPoll',
    //   {
    //     chat_id: chat_id,
    //     question: 'Поверим ему?',
    //     options: [
    //       'Да. он молодец',
    //       'Нет, он нагло врёт'
    //     ],
    //     is_anonymous: false,
    //     type: 'quiz',
    //     correct_option_id: 0,
    //     explanation: 'Ну и как ты теперь будешь в глаза ему смотреть?'
    //   }
    // );

    // sendMessage({
    //   chat_id: body.callback_query.message.chat.id,
    //   text: `command ${command}, transfer_id ${transfer_id}`,
    //   reply_markup: {
    //     inline_keyboard: [
    //       [
    //         {text: 'Отклонить', callback_data: `declineTransfer_${delivery_id}`},
    //         {text: 'Подтвердить', callback_data: `confirmTransfer_${delivery_id}`},
    //       ]
    //     ]
    //   }
    // });
  })
  .catch(function (error) {
    console.log('Catch ERROR RESPONSE DATA');
    console.log(error.response.data);
    console.log(JSON.stringify(error.response.data, null, 2));
    
    // console.log(error.response.data);

    // {
    //   "data": null,
    //   "error": {
    //     "status": 409,
    //     "name": "ConflictError",
    //     "message": "Нельзя создавать новые заявки на доставку, пока не подтверждены все предыдушие",
    //     "details": {
    //       "nonConfirmedTransfers": {
    //         "data": [
    //           {
    //             "id": 45,
    //             "attributes": {
    //               "date": "2022-07-30T18:47:12.381Z",
    //               "confirmed": false,
    //               "createdAt": "2022-07-30T18:47:12.385Z",
    //               "updatedAt": "2022-07-30T18:47:12.385Z",
    //               "confirmationThreshold": 3,
    //               "user": {
    //                 "data": {
    //                   "id": 7,
    //                   "attributes": {
    //                     "username": "Александр Туманов",

    // {
    //   data: null,
    //   error: {
    //     status: 500,
    //     name: 'InternalServerError',
    //     message: 'Internal Server Error'
    //   }
    // }

    let last_transfer = error.response.data.error.details.nonConfirmedTransfers.data[0];
    let last_user = last_transfer.attributes.user.data;

    if ('nonConfirmedTransfers' in error.response.data.error.details) {
      sendMessage({
        chat_id: chat_id,
        parse_mode: 'Markdown',
        text: `😥
${user.first_name} ${user.last_name}
${error.response.data.error.message}
Вначале подтвердите доставку №${last_transfer.id}, которую ранее создал ${last_user.attributes.username}.`,
        reply_markup: {
          inline_keyboard: [
            [
              // {text: 'Отклонить', callback_data: `declineTransfer_${delivery_id}`},
              {text: confirm_button_title, callback_data: `confirmTransfer_${last_transfer.id}`},
            ]
          ]
        }
      });
    }
    else {
      sendMessage({
        chat_id: chat_id,
        parse_mode: 'Markdown',
        text: `При создании записи о доставке бутылки для ${user.first_name}${user.last_name ? ' ' + user.last_name : ''} произошёл *сбой на сервере*.
Скорее всего для пользователя ${user.first_name}${user.last_name ? ' ' + user.last_name : ''} нет приязки к УЗ Страпи.
Обратитесь к администратору с доступом к Страпи.
Детали ошибки:
\`\`\`
${error.response && error.response.data ? JSON.stringify(error.response.data, null, 2) : '' }
\`\`\``
      });
    }
  });

  // Если пользователь не найден, то сказать ему об этом

  // Если привязка пользователя Телеграм к пользователю Страпи есть, то получить ответ о создании заявки
  // и оповестить пользователя
}

let confirmTransfer = (body) => {
  let user = body.callback_query.from;
  let chat = body.callback_query.message.chat;
  console.log('confirmTransfer', body.callback_query);

  let callback_query_id = body.callback_query.id;
  console.log('callback_query_id: ' + callback_query_id);
  let callback_query_data = body.callback_query.data;
  let data_array = callback_query_data.split('_');
  let command = data_array[0];
  let transfer_id = parseInt(data_array[1], 10);

  let sended_data = {
    "data": {
        "telegram_user_id": user.id,
        "transfer": transfer_id,
    }
  };

//   sendMessage({
//     chat_id: body.callback_query.message.chat.id,
//     parse_mode: 'Markdown',
//     text: `Пользователь ${user.first_name} ${user.last_name} нажал кнопку "Подтвердить" для доставки ${transfer_id}.
// Отправлено в бекенд.
// \`\`\`
// ${JSON.stringify(sended_data, null, 2)}
// \`\`\``,
//   });

  axios.post(
    `${BACKEND_API_URL}/confirmations/bybot?populate=transfer`,
    sended_data,
    {
      headers: {
        Authorization:
          `Bearer ${BACKEND_BOT_TOKEN}`,
      }
    }
  )
  .then(function (response) {
    console.log('esponse.data');
    console.log(response.data);
    // Отправил число как строку.
    // {
    //   data: null,
    //   error: {
    //     status: 400,
    //     name: 'BadRequestError',
    //     message: 'Недопустимое значение transfer',
    //     details: 'Допустимые значения transfer: целое из интервала 1..9007199254740991'
    //   }
    // }
    // Попробовал подтвердить свою собственную доставку.
    // {
    //   data: null,
    //   error: {
    //     status: 409,
    //     name: 'ConflictError',
    //     message: 'Вы не можете подтверждать свои собственные доставки',
    //     details: 'Попробуйте делать приятно другим, а не себе :)'
    //   }
    // }
    // Подтверждение (без закрытия)
    // {
    //   "data": {
    //     "id": 20,
    //     "attributes": {
    //       "confirmed": true,
    //       "createdAt": "2022-07-28T19:01:28.632Z",
    //       "updatedAt": "2022-07-28T19:01:28.632Z",
    //       "transfer": {
    //         "data": {
    //           "id": 35,
    //           "attributes": {
    //             "date": "2022-07-28T18:09:53.946Z",
    //             "confirmed": false,
    //             "createdAt": "2022-07-28T18:09:53.953Z",
    //             "updatedAt": "2022-07-28T18:09:53.953Z",
    //             "confirmationThreshold": 4
    //           }
    //         }
    //       }
    //     }
    //   },
    //   "meta": {
    //     "notBlockedOtherUsersNumber": 4,
    //     "positiveConfirmationsNumber": 3
    //   }
    // }

    let transfer = response.data.data.attributes.transfer;

    // Пользователь ${user.first_name} ${user.last_name} нажал кнопку "Подтвердить" для доставки ${transfer_id}.
// Отправлено в бе
    let transfer_text = `Получено подтверждение от ${user.first_name} ${user.last_name} по доставке ${transfer.data.id}`;

    if (transfer.data.attributes.confirmed) {
      sendMessage({
        chat_id: chat.id,
        parse_mode: 'Markdown',
        text: `${transfer_text}
Доставка подтверждена. Больше подтверждений не требуется.`,
      });

      sendSticker({
        chat_id: chat.id,
        sticker: 'CAACAgIAAxkBAAIB1WLQgihV_XASYEz7yICUEfG8AZfwAAJAAANSiZEjNVy6P3rWQokpBA'
      })
    }
    else {
      let meta = response.data.meta;
      let votes = {
        max:      parseInt(meta.notBlockedOtherUsersNumber, 10),
        min:      parseInt(meta.confirmationThreshold, 10),
        for:      parseInt(meta.positiveConfirmationsNumber, 10),
        against:  0
      }

      sendMessage({
        chat_id: chat.id,
        parse_mode: 'Markdown',
        text: `${transfer_text}
Всего: *${votes.for}*
Требуется: *${votes.min}*
Подтверждающих: *${votes.max}*`,
      });
    }
  })
  .catch(function (error) {
    if (
      'response' in error
      && 'data' in error.response
      && 'error' in error.response.data
    ) {
      console.log('error.response.data');
      console.log(error.response.data);
      
      let error_data = error.response.data.error;
      // 😥
      // {
      //   "status": 409,
      //   "name": "ConflictError",
      //   "message": "Вы уже подтвердили эту доставку",
      //   "details": "Астановитесь!"
      // }
      sendMessage({
        chat_id: body.callback_query.message.chat.id,
        parse_mode: 'Markdown',
        text: `😥
${user.first_name} ${user.last_name}
${error_data.message}${error_data.details ? "\n" + error_data.details : ''}`,
      });
    }
    else {
      sendMessage({
        chat_id: body.callback_query.message.chat.id,
        parse_mode: 'Markdown',
        text: `Возникла ошибка:
\`\`\`
${JSON.stringify(error, null, 2)}
\`\`\``,
      });
    }
  });
}

let declineTransfer = (body) => {
  let user = body.callback_query.from;
  console.log('Функция declineTransfer');
  console.log('body.callback_query');
  console.log(body.callback_query);

  let callback_query_id = body.callback_query.id;
  console.log('callback_query_id: ' + callback_query_id);
  let callback_query_data = body.callback_query.data;
  let data_array = callback_query_data.split('_');
  let command = data_array[0];
  let transfer_id = parseInt(data_array[1], 10);

  let sended_data = {
    "data": {
        "telegram_user_id": user.id,
        "transfer": transfer_id,
        "confirmed":  false,
    }
  };

  sendMessage({
    chat_id: body.callback_query.message.chat.id,
    parse_mode: 'Markdown',
    text: `Пользователь ${user.first_name} ${user.last_name} нажал кнопку "Отклонить" для доставки ${transfer_id}.
Отправлено в бекенд.
\`\`\`
${JSON.stringify(sended_data, null, 2)}
\`\`\``,
  });

  axios.post(
    `${BACKEND_API_URL}/confirmations/bybot?populate=transfer`,
    sended_data,
    {
      headers: {
        Authorization:
          `Bearer ${BACKEND_BOT_TOKEN}`,
      }
    }
  )
  .then(function (response) {
    console.log('esponse.data');
    console.log(response.data);
    sendMessage({
      chat_id: body.callback_query.message.chat.id,
      parse_mode: 'Markdown',
      text: `Получено с бекенда.
\`\`\`
${JSON.stringify(response.data, null, 2)}
\`\`\``,
    });
  })
  .catch(function (error) {
    console.log('error.response.data');
    console.log(error.response.data);

    if ('error' in error.response.data) {
      let error_data = error.response.data.error;

      sendMessage({
        chat_id: body.callback_query.message.chat.id,
        parse_mode: 'Markdown',
        text: `Получено с бекенда.
\`\`\`
${JSON.stringify(error_data, null, 2)}
\`\`\``,
      });
    }
  });
}

let getTransfers = (body) => {
  axios.get(`${BACKEND_API_URL}/transfers?populate=*&sort[0]=confirmed:asc&sort[1]=date:desc`, {
      headers: {
        Authorization:
          `Bearer ${BACKEND_BOT_TOKEN}`,
      }
    }
  )
  .then(function (response) {
    console.log(response.data);
    let text = '';
    // ОБРАЗЕЦ ДАННЫХ ПО ОДНОЙ ДОСТАВКЕ
    // {
    //   id: 1,
    //   attributes: {
    //     date: '2022-07-09T10:04:00.000Z',
    //     confirmed: true,
    //     createdAt: '2022-07-10T01:56:26.317Z',
    //     updatedAt: '2022-07-12T11:00:42.609Z',
    //     confirmationThreshold: null
    //   }
    // }
    if (response.data.data.length == 0) {
      sendMessage({
        chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
        text: 'Тут пока пусто. Пора идти за водой)',
        parse_mode: 'Markdown',
      });

      // getMenu(body);

      return;
    }

    for (let i = 0; i < response.data.data.length; i++) {
      console.log(response.data.data[i]);
      let formattedDate = '';
      try {
        let date = new Date(response.data.data[i].attributes.date);
        formattedDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`
      }
      catch(err) {
        formattedDate = `${response.data.data[i].attributes.date}`;
      }

      let delivery_id = response.data.data[i].id;
      let courier_name = response.data.data[i].attributes.user.data.attributes.username;
      let confirmed = response.data.data[i].attributes.confirmed;
      let confirmed_description = confirmed ? '👍 Подтверждено' :
            confirmed !== null ? '🤔 НЕ ПОДТВЕРЖДЕНО' : '😡 NULL (пожаловаться админу)';

      text += `Доставку №${delivery_id} от ${formattedDate} нёс ${courier_name}
${confirmed_description}\n`;
      if (i !== response.data.data.length - 1) {
        text += "---\n";
      } 
    }
    sendMessage({
      chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
      text: text,
    });
  })
  .catch(function (error) {
    console.log(error);
  })
}

let getMenu = (body) => {
  sendMessage({
    chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
    text: "Выберите действие:",
    reply_markup: {
      inline_keyboard: [
        [{text: 'Добавить доставку воды', callback_data: 'createTransfer'}],
        [{text: 'Получить список доставок', callback_data: 'getTransfers'}],
        [{text: 'Получить статистику', callback_data: 'getStatistics'}],
        [{text: 'Получить информацию о проекте', callback_data: 'getInfo'}],
        [{text: 'Перейти на сайт Водоноса', url: 'https://vodonos.roboto.pro/'}]
      ]
    }
  });
}

let getInfo = (body) => {
  sendMessage({
    chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
    text: "Проект по фиксированию фактов приноса воды для справедливого распределения нагрузки на офисных водоносов.",
  });
}

let getStatistics = (body) => {
  console.log('getStatistics()');
  axios.get('https://vodonos.roboto.pro/strapi/api/statistics' +
            '?fields=totalTransfers,confirmedTransfers' +
            '&populate[user][fields]=username' +
            '&pagination[pageSize]=10' +
            '&sort=confirmedTransfers:desc' +
            '&filters[user][blocked]=false', {
      headers: {
        Authorization:
          `Bearer ${BACKEND_BOT_TOKEN}`,
      }
    }
  )
  .then(function (response) {
    console.log('THEN');
    console.log(JSON.stringify(response.data, null, 2));
    let text = 'Статистика по доставке воды:';
    let usersStats = response.data.data;
    let lastUserIndex = usersStats.length - 1;

    if (usersStats.length == 0) {
      sendMessage({
        chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
        text: 'У всех *по нулям*) Может ты хочешь стать первым?)',
        parse_mode: 'Markdown',
      });

      // getMenu(body);

      return;
    }

    for (let i = 0; i < usersStats.length; i++) {
      let user = usersStats[i].attributes.user;
      let deliveredCount = usersStats[i].attributes.totalTransfers;
      let confirmedCount = usersStats[i].attributes.confirmedTransfers;
      let confirmedCountText = deliveredCount != confirmedCount ? ', не подтверждено *' + String(deliveredCount - confirmedCount) + '*' : '';
      let mark = i === 0 ? ' ⭐️' : (i === lastUserIndex ? ' 🤨' : '');
      
      text += `\n ${user.data.attributes.username}: доставлено *${deliveredCount}*${confirmedCountText}${mark}`
    }
    // {
    //   "data": [
    //     {
    //       "id": 7,
    //       "attributes": {
    //         "confirmedTransfers": 1,
    //         "totalTransfers": 1,
    //         "user": {
    //           "data": {
    //             "id": 3,
    //             "attributes": {
    //               "username": "Юлия Редина"
    //             }
    //           }
    //         }
    //       }
    sendMessage({
      chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
      text: text,
      parse_mode: 'Markdown',
      reply_markup: {
        inline_keyboard: [
          [{text: 'Получить список доставок', callback_data: 'getTransfers'}]
        ]
      }
    });
  })
  .catch(function (error) {
    console.log(JSON.stringify(error, null, 2));
    // data: {
    //   data: null,
    //   error: {
    //     status: 401,
    //     name: 'UnauthorizedError',
    //     message: 'Missing or invalid credentials',
    //     details: {}
    //   }
    // }

    sendMessage({
      chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
      parse_mode: 'Markdown',
      text: `Получено с бекенда.
\`\`\`
${JSON.stringify(error, null, 2)}
\`\`\``,
    });
  })
}

app.get('/', (req, res) => {
  console.log(req.params);
  res.send('Hello World');
});

app.post('/', (req, res) => {
  console.log('/ параметры');
  console.log(req.params);
  // console.log(req.body);
  res.send('Hello World');
});

// Обработчик GET нужен только для проверки доступности пути /webhook.
app.get('/webhook', (req, res) => {
  console.log('/webhook параметры запроса');
  console.log(req.query);

  res.type('text/plain');
  res.send('Путь /webhook доступен');
});

// Сервер Телеграм присылает всё через POST.
// В req.params попадает то, что записывается через ":", например, /webhook/:test_path, добавит test_path.
// В req.query попадает то, что идёт в части запроса в строке URL после "?"
// В req.body попадает то, что передаётся в теле POST-запроса
app.post('/webhook', (req, res) => {
  console.log('WH POST');
  console.log(req.body);

  if (req.body.message) {
    let chat_id = req.body.message.chat.id;
    if (
      chat_id !== -799987486      // чат разработчиков Водоноса
      && chat_id !== -752838370   // чат Water gangbang
      && chat_id !== 283943071    // чат Александра Туманова
    ) {
      sendMessage({
        chat_id: chat_id,
        text: 'Бот находится в стадии разработки. По всем вопросам о боте вы можете обратиться к @justice1980',
        reply_markup: {
          inline_keyboard: [
            [{text: 'Перейти на сайт Водоноса', url: 'https://vodonos.roboto.pro/'}]
          ]
        }
      });

      res.send('Hello World');
      return;
    }

    // Если в строке текста передавали какие-нибудь команды, то они попадают в массив entities
    if (
      req.body.message.entities
      && req.body.message.entities.length !== 0
    ) {
      // В массив req.body.message.entities попадают все строки, которые сервер интерпретирует,
      // как один из специальных типов:
      // mention - упоминание пользователя (@username)
      // hashtag - хештег (#hashtag)
      // cashtag - кештег или код валюты ($USD)
      // bot_command - команда для бота (/menu@vodonos_roboto_pro_bot или просто /menu)
      // url - URL (https://vodonos.roboto.pro/)
      // email - адрес электронной почты (vodonos@roboto.pro)
      // phone_number - номер телефона (+7(987)123-4567)
      // bold - жирный шрифт
      // italic - наклонный шрифт
      // underline - подчёркнутый текст
      // strikethrough - зачёркнутый текст
      // spoiler - spoiler message (замазанное сообщение)
      // code - моноширинная строка текста (форматирование через меню клиента)
      // pre - моноширинный блок текста (```)
      // text_link - для ссылок, у которых не виден URL (форматирование через меню клиента)
      // text_mention - имя пользователя, которого не существует.
      let entities = req.body.message.entities;
      console.log(req.body.message.entities);

      for (let i = 0; i < req.body.message.entities.length; i++) {
        console.log(i + ' ' + entities[i].type);
        if (entities[i].type === 'bot_command') {
          console.log('Запись: ' + i);
          console.log(entities[i]);
          console.log('offset: ' + entities[i].offset);
          console.log('length: ' + entities[i]['length']);
          console.log('Строка: ' + req.body.message.text);
          let bot_command = req.body.message.text.substring(entities[i].offset, entities[i].offset + entities[i].length);
          console.log('bot_command: ' + bot_command);
// Список команд для @BotFather
// get_my_telegram_id - Показать ID пользователя Телеграм
// get_menu - Меню бота
// add_transfer - Добавить доставку воды
// get_transfers - Получить все доставки
// get_statistics - Получить статистику
          // Если серверу Телеграм ничего не возвращать, а выходить из функции с помощью return, то
          // сервер Телеграм будет считать, что его сообщение НЕ доставлено и будет пытаться отправить
          // его повторно каждые две минуты.
          switch(bot_command) {
            // Стандартная команда запуска бота
            case '/start':
              // Ничего не делать
            break;

            // Показать ID пользователя Телеграм
            case '/get_my_telegram_id':
            case '/get_my_telegram_id@vodonos_roboto_pro_bot':
              console.log('КОМАНДА /get_my_telegram_id');
              let first_name = req.body.message.from.first_name;
              let last_name = req.body.message.from.last_name ? ' ' + req.body.message.from.last_name : '';
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: `${first_name}${last_name}, ваш идентификатор Телеграм: ${req.body.message.from.id}`
              });
            break;

            // Меню бота
            case '/get_menu':
            case '/get_menu@vodonos_roboto_pro_bot':
              console.log('Запрошено меню');
              getMenu(req.body);
            break;
      
            // Добавить доставку воды
            case '/add_transfer':
            case '/add_transfer@vodonos_roboto_pro_bot':
              console.log('Доставка бутылки');
              createTransfer(req.body);
            break;

            // Получить все доставки
            case '/get_transfers':
            case '/get_transfers@vodonos_roboto_pro_bot':
              console.log('Доставка бутылки');
              getTransfers(req.body);
            break;

            // Получить статистику
            case '/get_statistics':
            case '/get_statistics@vodonos_roboto_pro_bot':
              getStatistics(req.body);
            break;
      
            default:
              console.log('Команда ' + bot_command + ' не распознана');
              console.log(req.body.message.text);
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: 'Команда ' + bot_command + ' не распознана'
              });

              // Отладка
              if (req.body.message.chat.id != 283943071) {
                sendMessage({
                  chat_id: "283943071",
                  text: 'Команда ' + bot_command + ' не распознана'
                });
              }
          }
        }
      }
    }
  }
  else if (req.body.callback_query) {
    let callback_query_id = req.body.callback_query.id;
    console.log('callback_query_id: ' + callback_query_id);
    let callback_query_data = req.body.callback_query.data;
    let command = callback_query_data.split('_')[0];
    switch (command) {
      case 'createTransfer':
        createTransfer(req.body);
      break;

      case 'getTransfers':
        getTransfers(req.body);
      break;

      case 'confirmTransfer':
        confirmTransfer(req.body);
      break;

      case 'declineTransfer':
        declineTransfer(req.body);
      break;

      case 'getInfo':
        getInfo(req.body);
      break;

      case 'getStatistics':
        getStatistics(req.body);
      break;

      default:
        sendMessage({
          chat_id: "283943071",
          text: "Кнопка не распознана"
        });
    }

    axios.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/answerCallbackQuery`, {
      callback_query_id: callback_query_id,
      text: req.body.callback_query.data
    })
    .then(function (response) {
      console.log('response');
    })
    .catch(function (error) {
      console.log('error');
    });
  }
  res.send('wh');
});


app.get('/transfer', function (req, res) {
  res.type('text/plain');
  res.send('Схожу за водичкой');
});

app.get('/confirm', function (req, res) {
  res.type('text/plain');
  res.send('Подтвержение получено');
});

// пользовательская страница 404
app.use(function(req, res, next){
  res.type('text/plain');
  res.status(404);
  res.send('404 — Не найдено');
});

app.listen(BOT_PORT, BOT_INTERFACE);
console.log(`running on http://${BOT_INTERFACE}:${BOT_PORT}`);
