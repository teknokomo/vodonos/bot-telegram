# bot-telegram
[[_TOC_]]

## Инструкция для администратора

### Запуск в Docker
```shell
read -p 'Введите ключ бота для Телеграм: ' TELEGRAM_BOT_TOKEN
export TELEGRAM_BOT_TOKEN
read -p 'Введите URL API бекенда: ' BACKEND_API_URL
export BACKEND_API_URL
read -p 'Введите ключ бота для бекенда: ' BACKEND_BOT_TOKEN
export BACKEND_BOT_TOKEN=$BACKEND_BOT_TOKEN
docker run \
     --name vodonos-bot-telegram \
     -e DEBUG=express \
     -e TELEGRAM_BOT_TOKEN=$TELEGRAM_BOT_TOKEN \
     -e BACKEND_API_URL=$BACKEND_API_URL \
     -e BACKEND_BOT_TOKEN=$BACKEND_BOT_TOKEN \
     -e BOT_INTERFACE='0.0.0.0' \
     -e BOT_PORT=3000 \
     -p 127.0.0.1:3000:3000 \
     -d \
     registry.gitlab.com/teknokomo/vodonos/bot-telegram
```

## Инструкци для разработчика

### Сборка образа
```shell
docker build -t registry.gitlab.com/teknokomo/vodonos/bot-telegram .
```

### Отправка образа в хранилище Gitlab
```shell
docker login registry.gitlab.com
docker push registry.gitlab.com/teknokomo/vodonos/bot-telegram
```

### Запуск в Docker
```shell
read -p 'Введите ключ бота для Телеграм: ' TELEGRAM_BOT_TOKEN
export TELEGRAM_BOT_TOKEN
read -p 'Введите URL API бекенда: ' BACKEND_API_URL
export BACKEND_API_URL
read -p 'Введите ключ бота для бекенда: ' BACKEND_BOT_TOKEN
export BACKEND_BOT_TOKEN
docker run \
     --name vodonos-bot-telegram \
     --env DEBUG=express \
     --env TELEGRAM_BOT_TOKEN=$TELEGRAM_BOT_TOKEN \
     --env BACKEND_API_URL=$BACKEND_API_URL \
     --env BACKEND_BOT_TOKEN=$BACKEND_BOT_TOKEN \
     --env BOT_INTERFACE='0.0.0.0' \
     --network vodonos-network \
     --publish 127.0.0.1:3000:3000 \
     --detach \
     registry.gitlab.com/teknokomo/vodonos/bot-telegram
```

## Отправка запросов и сообщений в Телеграм
Оповещение водоносов через Телеграм

[Создать бота](https://core.telegram.org/bots#6-botfather)

Получить API-токен бота от @BotFather

Добавить бота в чат в который вы будете передавать сообщения

Получить обновлённые данные для бота и посмотреть ID чата (отрицательное целое число):
```
curl https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/getUpdates | jq
```
Отправить сообщение через HTTP API: https://core.telegram.org/bots/api#sendmessage
```
curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"chat_id": "123456789", "text": "This is a test from curl", "disable_notification": true}' \
     https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage
```

## Полное описание API
https://core.telegram.org/bots/api

## Запуск всего проекта
```shell
docker-compose up -d
```
## Запуск всего проекта для отладки (без бота)
Для начала надо скачать репу бекенда и локально пересобрать образ бекенда правильно значение переменной окружения STRAPI_URL:
```shell
export STRAPI_URL='http://localhost:1337'
```
После этого создать и запустить все контейнеры кроме бота:
```shell
docker-compose up -d vodonos-dbms-postgresql vodonos-backend-strapi
```